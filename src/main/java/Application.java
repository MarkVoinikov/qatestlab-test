import models.Task;
import service.DateChecker;
import service.TaskService;

import java.time.LocalDate;

import static models.TaskStatus.CREATED;
import static models.TaskStatus.FINISHED;
import static models.TaskStatus.IN_PROGRESS;

public class Application {
    public static void main(String[] args) {
        TaskService taskService = new TaskService();
        DateChecker dateChecker = new DateChecker();
        Task task1 = new Task("task1", FINISHED, LocalDate.of(2019, 10, 7));
        Task task2 = new Task("task2", CREATED, LocalDate.of(2019, 10, 8));
        Task task3 = new Task("task3", CREATED, LocalDate.of(2019, 10, 7));

        taskService.saveTask(task1);
        taskService.saveTask(task2);
        taskService.saveTask(task3);

        dateChecker.checker(taskService.getAllTasks());

        task1.setStatus(IN_PROGRESS);
        taskService.updateTask(task1);

        taskService.getTaskByStatus(IN_PROGRESS).forEach(System.out::println);



    }
}
