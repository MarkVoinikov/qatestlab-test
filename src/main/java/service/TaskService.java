package service;

import dao.TaskDao;
import models.Task;
import models.TaskStatus;

import java.util.List;
import java.util.Optional;

public class TaskService {
    private TaskDao taskDao = new TaskDao();
    public TaskService() {
    }

    public Optional<Task> getTask(int id) {
        return taskDao.get(id);
    }

    public List<Task> getTaskByStatus(TaskStatus status) {
        return taskDao.getByStatus(status);
    }

    public List<Task> getAllTasks() {
        return taskDao.getAll();
    }

    public void saveTask(Task task) {
        taskDao.save(task);
    }

    public void updateTask(Task task) {
        taskDao.update(task);
    }

    public void deleteTask(Task task) {
        taskDao.delete(task);
    }
}
