package service;

import models.Task;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class DateChecker {

    DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private LocalDate now = LocalDate.now();

    public void checker(List<Task> taskList) {
        for (Task task : taskList) {
            if (task.getDate().equals(now)) {
                System.out.println("This task can be done today: " + '\'' + task.toString());
            }
        }
    }
}
