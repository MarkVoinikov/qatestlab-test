package dao;

import models.Task;
import models.TaskStatus;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import utils.HibernateSessionFactoryUtil;

import java.util.List;
import java.util.Optional;

public class TaskDao implements TasksDaoInterface<Task> {
    public TaskDao() {
    }

    @Override
    public Optional<Task> get(int id) {
        return Optional.ofNullable(HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Task.class, id));
    }

    @Override
    public List<Task> getByStatus(TaskStatus status) {
        return (List<Task>) HibernateSessionFactoryUtil.getSessionFactory().openSession().createCriteria(Task.class)
                .add(Restrictions.eq("status", status)).list();
    }

    @Override
    public List<Task> getAll() {
        return (List<Task>) HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From Task").list();
    }

    @Override
    public void save(Task task) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(task);
        transaction.commit();
        session.close();
    }

    @Override
    public void update(Task task) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(task);
        transaction.commit();
        session.close();
    }

    @Override
    public void delete(Task task) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(task);
        transaction.commit();
        session.close();
    }
}
