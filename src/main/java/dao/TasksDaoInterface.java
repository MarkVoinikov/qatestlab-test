package dao;

import models.TaskStatus;

import java.util.List;
import java.util.Optional;

public interface TasksDaoInterface<T> {
    Optional<T> get(int id);
    List<T> getByStatus(TaskStatus status);
    List<T> getAll();
    void save(T t);
    void update(T t);
    void delete(T t);
}
