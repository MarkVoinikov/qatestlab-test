package models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table (name = "tasks")

public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition="enum('CREATED','IN_PROGRESS','FINISHED')")
    private TaskStatus status;
    @Column
    private LocalDate date;

    public Task() {
    }

    public Task(String name, TaskStatus status, LocalDate date){
        this.name = name;
        this.status = status;
        this.date = date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status=" + status + '\'' +
                ", date='" + date +
                '}';
    }
}
