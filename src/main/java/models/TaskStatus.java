package models;

public enum TaskStatus {
    CREATED,
    IN_PROGRESS,
    FINISHED;
}
